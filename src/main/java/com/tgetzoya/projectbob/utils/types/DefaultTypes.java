/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * DefaultTypes.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.utils.types;

/**
 * Default types for classes.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class DefaultTypes {
    /**
     * The hash type for hasing digest.
     */
    public static final String DIGEST_TYPE = "SHA-512";

    /**
     * The string encoding type.
     */
    public static final String STRING_ENC_TYPE = "UTF-8";

    /**
     * Minimum number of bytes allowed for a salt.
     */
    public static final int MIN_SALT_SIZE = 16;
}
