/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * PasswordUtils.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.utils.password;

import com.tgetzoya.projectbob.utils.types.DefaultTypes;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utility for creating passwords.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class PasswordUtils {
    /**
     * Generates a password hash for the user-entered password.
     *
     * @param password the user-entered password
     * @param salt     the salt to use with the password
     * @return the hashed password
     * @throws NoSuchAlgorithmException     when the algorithm isn't available
     * @throws UnsupportedEncodingException when the string encoding type isn't available
     */
    public static ByteBuffer generatePasswordHash(ByteBuffer password, ByteBuffer salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance(DefaultTypes.DIGEST_TYPE);
        md.update(salt);
        return ByteBuffer.wrap(md.digest(password.array()));
    }
}
