/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * AuthUtils.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.utils.auth;

import com.tgetzoya.projectbob.utils.salt.SaltUtils;
import com.tgetzoya.projectbob.utils.types.DefaultTypes;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Date;

/**
 * Utility for creating authentication tokens.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class AuthUtils {

    /**
     * Generate an authentication token based on id and other factors.
     *
     * @param id the user id
     * @return an authentication token string
     * @throws NoSuchAlgorithmException     if the digest type is not available
     * @throws UnsupportedEncodingException if the encoding type is not available
     */
    public static String generateAuthToken(String id) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md;

        md = MessageDigest.getInstance(DefaultTypes.DIGEST_TYPE);
        md.update(SaltUtils.generateSalt());
        md.update((new Date()).toString().getBytes());
        md.update(id.getBytes(DefaultTypes.STRING_ENC_TYPE));

        return Base64.getEncoder().encodeToString(md.digest());
    }
}
