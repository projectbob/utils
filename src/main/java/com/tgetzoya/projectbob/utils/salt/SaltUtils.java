/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * SaltUtils.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.utils.salt;

import com.tgetzoya.projectbob.utils.types.DefaultTypes;

import java.nio.ByteBuffer;
import java.security.SecureRandom;

/**
 * Utilities for generating salt values.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class SaltUtils {
    /**
     * Generates a salt with the number of bytes defined by
     * {@link com.tgetzoya.projectbob.utils.types.DefaultTypes#MIN_SALT_SIZE}.
     *
     * @return a ByteBuffer with the salt
     */
    public static ByteBuffer generateSalt() {
        return generateSalt(DefaultTypes.MIN_SALT_SIZE);
    }

    /**
     * Generates a salt with a specified number of bytes.  The minimum number of
     * bytes allowed is 16.
     *
     * @param byteSize the number of bytes to generate a salt
     * @return a ByteBuffer with the salt
     */
    public static ByteBuffer generateSalt(int byteSize) {
        if (byteSize < DefaultTypes.MIN_SALT_SIZE) {
            byteSize = DefaultTypes.MIN_SALT_SIZE;
        }

        SecureRandom secureRandom = new SecureRandom();

        byte[] salt = new byte[byteSize];
        secureRandom.nextBytes(salt);

        return ByteBuffer.wrap(salt);
    }
}
