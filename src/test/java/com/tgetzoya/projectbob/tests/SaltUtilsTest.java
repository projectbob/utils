/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * SaltUtilsTest.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.tests;

import com.tgetzoya.projectbob.utils.salt.SaltUtils;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;

/**
 * @author Thomas Getzoyan
 * @versin 1.0
 * @since 1.0
 */
public class SaltUtilsTest {

    @Test
    public void generateSalt() {
        Assert.assertNotNull(SaltUtils.generateSalt());
    }

    @Test
    public void generateSaleWithLessThan16Bytes() {
        ByteBuffer salt = SaltUtils.generateSalt(12);
        Assert.assertEquals(salt.array().length, 16);
    }

    @Test
    public void generateSaleWithLessThan0Value() {
        ByteBuffer salt = SaltUtils.generateSalt(Integer.MIN_VALUE);
        Assert.assertEquals(salt.array().length, 16);
    }

    @Test
    public void generateSaleWithGreaterThan16Value() {
        ByteBuffer salt = SaltUtils.generateSalt(42);
        Assert.assertEquals(salt.array().length, 42);
    }

    @Test(expected = OutOfMemoryError.class)
    public void generateSaleWithMaxIntegerValue() {
        ByteBuffer salt = SaltUtils.generateSalt(Integer.MAX_VALUE);
        Assert.assertEquals(salt.array().length, Integer.MAX_VALUE);
    }
}
