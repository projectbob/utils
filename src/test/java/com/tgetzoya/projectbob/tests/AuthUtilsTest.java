/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * com.tgetzoya.projectbob.tests.AuthUtilsTest.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/

package com.tgetzoya.projectbob.tests;

import com.tgetzoya.projectbob.utils.auth.AuthUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Tests for AuthUtils.
 *
 * @author Thomas Getzoyan
 * @versin 1.0
 * @since 1.0
 */
public class AuthUtilsTest {

    @Test
    public void simpleAuthTest() {
        String token = null;

        try {
            token = AuthUtils.generateAuthToken("SampleToken");
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            Assert.fail(e.getMessage());
        }

        Assert.assertNotNull(token);
    }
}
