/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * PasswordUtilsTest.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.tests;

import com.tgetzoya.projectbob.utils.password.PasswordUtils;
import com.tgetzoya.projectbob.utils.salt.SaltUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;

/**
 * Tests for password utils.
 *
 * @author Thomas Getzoyan
 * @versin 1.0
 * @since 1.0
 */
public class PasswordUtilsTest {

    @Test
    public void testCreatePassword() {
        ByteBuffer pass = ByteBuffer.wrap("TestPassword".getBytes());
        ByteBuffer salt = SaltUtils.generateSalt();
        ByteBuffer result = null;

        try {
            result = PasswordUtils.generatePasswordHash(pass, salt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            Assert.fail(e.getMessage());
        }

        Assert.assertNotNull(result);
    }

    @Test(expected = NullPointerException.class)
    public void testNoSalt() {
        ByteBuffer pass = ByteBuffer.wrap("TestPassword".getBytes());
        ByteBuffer salt = null;

        try {
            PasswordUtils.generatePasswordHash(pass, salt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expected = NullPointerException.class)
    public void testNoPassword() {
        ByteBuffer pass = null;
        ByteBuffer salt = SaltUtils.generateSalt();

        try {
            PasswordUtils.generatePasswordHash(pass, salt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expected = NullPointerException.class)
    public void testNoData() {
        ByteBuffer pass = null;
        ByteBuffer salt = null;

        try {
            PasswordUtils.generatePasswordHash(pass, salt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            Assert.fail(e.getMessage());
        }
    }
}
